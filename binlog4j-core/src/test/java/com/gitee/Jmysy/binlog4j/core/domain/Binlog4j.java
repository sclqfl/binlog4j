package com.gitee.Jmysy.binlog4j.core.domain;

import lombok.Data;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.BitSet;

@Data
public class Binlog4j {

    private String username;

    private String password;

    private String remark;

    private Boolean sex;

    private LocalDateTime createTime;

    private Date createDate;

    private LocalDateTime updateTime;

    private LocalDate updateDate;

    private String longRemark;

    private BitSet bit;

    private byte[] byteRemark;


}
