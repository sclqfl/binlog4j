package com.gitee.Jmysy.binlog4j.springboot.starter;

import com.gitee.Jmysy.binlog4j.core.config.RedisConfig;
import com.gitee.Jmysy.binlog4j.core.position.BinlogPositionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@AutoConfigureOrder(10)
@EnableConfigurationProperties(Binlog4jAutoProperties.class)
@ConditionalOnProperty(prefix = "spring.binlog4j", name = "enable", havingValue = "true", matchIfMissing = true)
public class Binlog4jAutoConfiguration {

    @Autowired(required = false)
    private BinlogPositionHandler positionHandler;

    @Bean
    public Binlog4jInitializationBeanProcessor binlog4jAutoInitializing(Binlog4jAutoProperties properties) {
        RedisConfig redisConfig = properties.getRedisConfig();
        properties.getClientConfigs().forEach((clientName, clientConfig) -> {
            if (positionHandler != null) {
                clientConfig.setPositionHandler(positionHandler);
            }
            if (redisConfig != null && clientConfig.getRedisConfig() == null) {
                clientConfig.setRedisConfig(redisConfig);
            }
        });
        return new Binlog4jInitializationBeanProcessor(properties.getClientConfigs());
    }
}
